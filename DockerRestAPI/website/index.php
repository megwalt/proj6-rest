<html>
    <head>
        <title>CIS 322 REST-api Brevets</title>
    </head>

    <body>
        <h1>Open and Close Times - All Brevets</h1>
        <ul>
            <?php
            $json = file_get_contents('http://brevets-service/listAll');
            $obj = json_decode($json);
	        $brevets = $obj->Brevets;
            foreach ($brevets as $b) {
                $open = $b->open;
                $close = $b->close;

                echo "<li> open time: ".$open.", close time: ".$close."</li>";
            }
            ?>
        </ul>
        <h1>Open Times - Top 3 Brevets</h1>
        <ul>
            <?php
            $json = file_get_contents('http://brevets-service/listOpen?top=3');
            $obj = json_decode($json);
	        $brevets = $obj->Openings;
            foreach ($brevets as $open) {
                echo "<li> open time: ".$open;
            }
            ?>
        </ul>
        <h1>Close Times - Top 3 Brevets</h1>
        <ul>
            <?php
            $json = file_get_contents('http://brevets-service/listClose?top=3');
            $obj = json_decode($json);
	        $brevets = $obj->Closings;
            foreach ($brevets as $close) {
                echo "<li> close time: ".$close;
            }
            ?>
        </ul>
    </body>
</html>
