"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import request, render_template, make_response
from flask_restful import Resource, Api
from pymongo import MongoClient

import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config


import logging

###
# Globals
###
app = flask.Flask(__name__)
api = Api(app)

CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.brevetsdb

###
# Resources
###

def _retrieve_items():
    """
    helper function to retrieve list of database entries
    """
    _items = db.brevetsdb.find()
    items = [item for item in _items]

    return items


def _clean_items():
    """
    helper function that retrieves brevet information from database,
    sorts them, and splices the list to be the length specified by the
    user in the url.

    returns a list of dictionaries.
    """
    items = _retrieve_items()
    items.sort(key=lambda item: item["miles"])  

    num = len(items)

    if request.args.get("top") != None:
        num = int(request.args.get("top"))
        if num < 0:
            num = 0
    
    if len(items) > 0:
        items = items[1:]
        items = items[:num]

    items.sort(key=lambda item: item["kms"])

    return items


class Brevet(Resource):
    def get(self):
        items = _clean_items()

        json_dict = {'Brevets': [{"open": str(item["open"]), "close":str(item["close"])} for item in items]}

        return json_dict

class BrevetOpen(Resource):
    def get(self):
        items = _clean_items()

        json_dict = {'Openings': [str(item["open"]) for item in items]}

        return json_dict

class BrevetClose(Resource):
    def get(self):
        items = _clean_items()

        json_dict = {'Closings': [str(item["close"]) for item in items]}

        return json_dict


class BrevetCSV(Resource):
    def get(self):
        items = _clean_items()

        csv = "open times, close times</br>"

        for item in items:
            csv += item["open"] + "," + item["close"] + "</br>"

        return make_response(csv)


class BrevetOpenCSV(Resource):
    def get(self):
        items = [item["open"] for item in _clean_items()]

        csv = "open times</br>"

        for item in items:
            csv += item + "</br>"

        return make_response(csv)


class BrevetCloseCSV(Resource):
    def get(self):
        items = [item["close"] for item in _clean_items()]
        
        csv = "close times</br>"

        for item in items:
            csv += item + "</br>"

        return make_response(csv)



api.add_resource(Brevet, "/listAll", "/listAll/json")
api.add_resource(BrevetOpen, "/listOpen", "/listOpen/json")
api.add_resource(BrevetClose, "/listClose", "/listClose/json")
api.add_resource(BrevetCSV, "/listAll/csv")
api.add_resource(BrevetOpenCSV, "/listOpen/csv")
api.add_resource(BrevetCloseCSV, "/listClose/csv")


###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")

    return flask.render_template('calc.html')


@app.route("/display")
def display():
    _items = db.brevetsdb.find()
    items = [item for item in _items]
    brevet_dist = {'kms': 0}

    items.sort(key=lambda item: item["miles"])

    if len(items) > 0:
        brevet_dist = items[0]
        app.logger.debug(brevet_dist)
        items = items[1:]

    items.sort(key=lambda item: item["kms"])
    
    return flask.render_template('display.html', items=items, bd=brevet_dist)
    


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    km = request.args.get('km', 999, type=float)
    brevets = request.args.get('brevets', type=int)
    start_time = request.args.get('start_time', type=str)
    arw_start = arrow.get(start_time).isoformat()
 
    open_time = acp_times.open_time(km, brevets, arw_start)
    close_time = acp_times.close_time(km, brevets, arw_start)
    result = {"open": open_time, "close": close_time}

    return flask.jsonify(result=result)


@app.route("/_submit", methods=["POST"])
def _submit():
    data = {
        'miles': request.form["miles"],
        'kms': request.form["kms"],
        'location': request.form["location"],
        'open': request.form["open"],
        'close': request.form["close"] 
    }
    db.brevetsdb.insert_one(data)

    return flask.jsonify(status=True)


@app.route("/_reset", methods=["POST"])
def _reset():
    global db
    db.brevetsdb.drop()
    db = client.brevetsdb
    
    return flask.jsonify(status=True)


@app.route("/_display", methods=["GET"])
def _display():
    _items = db.brevetsdb.find()
    items = [item for item in _items]

    status = (len(items) > 0)
    
    return flask.jsonify(status = status)



#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(80))
    app.run(port=80, host="0.0.0.0")
