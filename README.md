# Project 6: Brevet time calculator service

Simple listing service from project 5 stored in MongoDB database.

Revised by Megan Walter (mwalter2@uoregon.edu)
 

## ACP controle times

That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.   

The user will input a distance in miles or kilometers, an initial date and time, and using a dropdown option, specify the total brevet distance. As data is added or amended, the page will update using AJAX to calculate the open time and close time of each control point.

On clicking a submit button in the top right corner of the screen, the control points and times get sent the a database.

On clicking a display button, a new screen will display the control points and times stored in the database.

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). 

Additional background information is given here (https://rusa.org/pages/rulesForRiders). 

We are essentially replacing the calculator here (https://rusa.org/octime_acp.html). We can also use that calculator to clarify requirements and develop test data.  


## Additional Details

The algorithms for calculating the windows of time for which each control point is open will round the control point distance to the nearest kilometer (e.g. a control point at 34.4 kms will have the same open and close times as a control point at 34 km). Additionally, timestamps will be rounded to the nearest minute (e.g. if a control point closes at 13:09:31, it will be displayed in the table as closing at 13:10).

The display button will display the controle times in order regardless of the order that the user puts them in in the table. 

## Test Cases

### Range 

If a control point surpasses the specified total brevet distance by more than 20 percent, an error message will be shown to the user so that they can update their input.

If a control point is negative, an error message will be shown to the user so that they can update their input.

### Buttons 

If the user attempts to submit the control points with none of the cells in the table filled, the user will recieve an error message and be unable to submit.

If the user attempts to submit the control points while one of the table rows is displaying an error message, they will recieve an error message and be unable to submit.

If the user attempts to display the control points but nothing has been submitted to the database, they will recieve an error message and be unable to display. 

## New Functionality

This project is run in three containers, the mongodb database, the consumer program (index.php) run on port 5000:80, and the brevets application + api run on port 5001:80.

The consumer program is a static webpage that displays:
* The open and close times for all brevets in the database
* The open times for the first three brevets (in ascending order)
* The close times for the first three brevets (in ascending order)

* Added a RESTful service to expose what is stored in MongoDB and created the following three basic APIs:
    * "http://<host:port>/listAll" returns all open and close times in the database
    * "http://<host:port>/listOpenOnly" returns open times only
    * "http://<host:port>/listCloseOnly" returns close times only

* Enabled the use of two different representations: one in csv and one in json. JSON acts as the default representation for the above three basic APIs. 
    * "http://<host:port>/listAll/csv" returns all open and close times in CSV format
    * "http://<host:port>/listOpenOnly/csv" returns open times only in CSV format
    * "http://<host:port>/listCloseOnly/csv" returns close times only in CSV format

    * "http://<host:port>/listAll/json" returns all open and close times in JSON format
    * "http://<host:port>/listOpenOnly/json" returns open times only in JSON format
    * "http://<host:port>/listCloseOnly/json" returns close times only in JSON format

* Added a query parameter to get top "k" open and close times. For examples, see below.

    * "http://<host:port>/listOpenOnly/csv?top=3" returns top 3 open times only (in ascending order) in CSV format 
    * "http://<host:port>/listOpenOnly/json?top=5" returns top 5 open times only (in ascending order) in JSON format
    * "http://<host:port>/listCloseOnly/csv?top=6" returns top 5 close times only (in ascending order) in CSV format
    * "http://<host:port>/listCloseOnly/json?top=4" returns top 4 close times only (in ascending order) in JSON format


## Notes

For the top "k" open/close times query parameter, if the value entered is negative, zero times will be shown. If k is greater than the amount of brevets within the database, then all open/close times will be displayed.

